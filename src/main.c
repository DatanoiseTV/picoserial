/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Raspberry Pi (Trading) Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "bsp/board.h"
#include "tusb.h"

#include "picoprobe_config.h"
#include "probe.h"
#include "cdc_uart.h"
#include "cdc_uart2.h"
#include "get_serial.h"
#include "led.h"

// For MSD reset
#include "pico/bootrom.h"
#include "hardware/flash.h"

// UART0 for Picoprobe debug
// UART1 for picoprobe to target device

void tud_cdc_line_coding_cb(uint8_t itf, cdc_line_coding_t const* line_coding) {
    if (itf == 0)
        cdc_uart_line_coding(line_coding);
    else if (itf == 1)
        cdc_uart2_line_coding(line_coding);
}

volatile int8_t lastRTS = -1;
void tud_cdc_line_state_cb(uint8_t itf, bool dtr, bool rts)
{
   if (itf == 0) {
        if (-1 != lastRTS) {
                if (lastRTS && !rts) {
                        if (dtr)
                                reset_usb_boot(0, 0);
                        else
                                watchdog_reboot(0, 0, 0);
                }
        }
        lastRTS = rts;
   } else if(itf == 1) {
	 cdc_uart2_state_cb(dtr, rts);
   }
}

int main(void) {

    board_init();
    usb_serial_init();
    cdc_uart_init();
    cdc_uart2_init();
    tusb_init();
    probe_init();
    led_init();

    picoprobe_info("Welcome to Picoprobe!\n");

    while (1) {
        tud_task(); // tinyusb device task
        cdc_uart_task();
	cdc_uart2_task();
        probe_task();
        led_task();
    }

    return 0;
}
